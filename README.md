# README #


### What is this repository for? ###
This is the repository for the source files of Admin Arsenal mod for Ark:Survival Evolved.

### How do I get set up? ###
I think what I did was I created a new mod folder or copied the default mod folder in unreal editor (ark editor) and just check out the branch using git to the newly copied folder.
I used sourcetree.  It's really easy to use because it's a GUI for GIT.   

### Contribution guidelines ###
Basically, just fork my master branch. 

### Who do I talk to? ###
Yourself.  Who else?  
Nah but seriously, you don't have to talk to me.  But let me know if you have any questions.